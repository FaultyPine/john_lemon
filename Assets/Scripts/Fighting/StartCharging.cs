using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCharging : MonoBehaviour
{
    JohnAttacking attk;
    bool charging = false;
    
    public ParticleSystem ChargingParticleSystem;

    void Start()
    {
        attk = GetComponent<JohnAttacking>();
        attk.StartCharging += OnStartCharging;
    }

    void OnStartCharging() {
        ChargingParticleSystem.Play();
        ChargingParticleSystem.transform.position = transform.position + (Vector3.up * 1.1f);
        charging = true;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Space) && charging) {
            
            attk.IncrementCharge();
            if (attk.currentCharge >= 1f) {
                EndCharge();
                attk.FireCharge();
            }

        }
        else if (charging) {
            EndCharge();
        }

    }

    void EndCharge() {
        ChargingParticleSystem.Stop();
        ChargingParticleSystem.Clear();
        charging = false;
    }
}
