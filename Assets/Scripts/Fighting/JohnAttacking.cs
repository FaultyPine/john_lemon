using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JohnAttacking : MonoBehaviour
{

    public System.Action StartCharging;
    public System.Action FireCharge;

    public float cooldown;
    bool isCooldown = false;

    public float currentCharge = 0.1f;
    [SerializeField] float chargeIncrement = 0.005f;

    public void IncrementCharge() {
        if (currentCharge < 1f)
            currentCharge += chargeIncrement;
        Debug.Log(currentCharge);
    }

    void Start()
    {
        
    }

    void Update()
    {


        
        if (Input.GetKeyDown(KeyCode.Space) && !isCooldown) { //  press
            if (StartCharging != null)
                StartCharging();
        }
        else if (Input.GetKeyUp(KeyCode.Space) && !isCooldown) { //  release
            if (FireCharge != null) {
                FireCharge();
                isCooldown = true;
                StartCoroutine(Cooldown());
            }
            currentCharge = 0.1f;
        }
        
    }

    IEnumerator Cooldown() {
        yield return new WaitForSeconds(cooldown);
        isCooldown = false;
    }
}
