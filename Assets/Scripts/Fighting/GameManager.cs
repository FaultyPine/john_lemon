using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public int maxWaves = 3;
    int currentWave = 1;
    bool win = false;
    public GameObject ghostFightingPrefab;
    public GameObject waveCounterObj;
    public GameEnding gameEndingInstance;
    public Transform playerTsf;
    public GameObject ghostsHolder;

    public Transform[] Lanes;
    public Transform[] Waypoints;
    
    TextMeshProUGUI waveCounterTxt;

    void Awake() {
        waveCounterTxt = waveCounterObj.GetComponent<TextMeshProUGUI>();
    }

    void Start()
    {
        currentWave = 1;
        waveCounterTxt.text = "Wave: 1 / " + maxWaves.ToString();
        StartCoroutine(Waves());
    }

    void Update()
    {
        if (win) {
            gameEndingInstance.Win();
        }
    }


    void SpawnEnemy() {
        int randomLaneIdx = Random.Range(0, 3);
        GameObject ghost = Instantiate(ghostFightingPrefab, Lanes[randomLaneIdx].position, Quaternion.identity);
        WaypointPatrol wpPatrol = ghost.GetComponent<WaypointPatrol>();
        if (wpPatrol != null) {
            wpPatrol.waypoints = Waypoints;
        }
        Observer obs = ghost.GetComponentInChildren<Observer>();
        if (obs != null) {
            obs.player = playerTsf;
            obs.gameEnding = gameEndingInstance;
        }
        ghost.transform.parent = ghostsHolder.transform;
    }
    
    bool NewWave() {
        if (currentWave + 1 >= maxWaves) {
            win = true;
            waveCounterTxt.text = "You Win!";
            return true;
        }
        else {
            currentWave += 1;
            waveCounterTxt.text = "Wave: " + currentWave.ToString() + " / " + maxWaves.ToString();
            return false;
        }
    }

    IEnumerator Waves() {
        while (true) {
            for (int i = 0; i < 10; i++) {
                SpawnEnemy();
                yield return new WaitForSeconds(Random.Range(5f, 6f));
            }
            if (NewWave()) {
                yield break;
            }
        }
    }
}
