using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBallLogic : MonoBehaviour
{
    public float moveForwardStep = 0.01f;
    public float lifetimeSecs = 1.5f;

    Transform player;

    [SerializeField] ParticleSystem trail;

    Vector3 moveForward = Vector3.zero;

    void Start()
    {
        StartCoroutine(Lifetime());
        trail.Play();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        moveForward = player.forward;
    }

    void Update()
    {
        transform.Translate(moveForward * moveForwardStep, Space.World);
    }

    IEnumerator Lifetime() {
        yield return new WaitForSeconds(Vector3.Magnitude(transform.localScale));
        trail.Stop();
        Destroy(this.gameObject);
    }


}
