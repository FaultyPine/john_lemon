using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementFighting : MonoBehaviour
{

    public GameObject NavLeft;
    public GameObject NavCenter;
    public GameObject NavRight;

    public GameObject ghostsHolder;
    public GameObject JohnLemonRoot; // this actually has influence over where the lemon model is looking, this transform's rotation is overriden by anims

    void Start()
    {
        
    }

    void Update()
    {

        float inp = Input.GetAxisRaw("Vertical");
        if (inp > 0f) {
            AlignToGhost();
        }
        else  {
            transform.LookAt(NavCenter.transform);
            JohnLemonRoot.transform.LookAt(NavCenter.transform);
        }


    }

    void AlignToGhost() {
        float min = -1f;
        Transform nearestGhost = null;

        foreach (Transform ghost in ghostsHolder.transform) {
            float dist = Vector3.Distance(ghost.position, transform.position);
            if (min == -1f) {
                min = dist;
                nearestGhost = ghost;
            }
            else if (dist < min) {
                min = dist;
                nearestGhost = ghost;
            }
        }

        if (nearestGhost != null) {
            JohnLemonRoot.transform.LookAt(nearestGhost);
            transform.LookAt(nearestGhost);
            Debug.DrawLine(transform.position, nearestGhost.transform.position);
        }
    }
}
