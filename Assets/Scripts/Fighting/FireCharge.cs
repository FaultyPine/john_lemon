using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCharge : MonoBehaviour
{

    public GameObject energyBall;

    JohnAttacking attk;

    void Start()
    {
        attk = GetComponent<JohnAttacking>();
        attk.FireCharge += OnFireCharge;
    }

    void OnFireCharge() {
        Debug.Log("Fire Charge");
        GameObject ball = Instantiate(energyBall, transform.position + (Vector3.up * 1.2f), transform.rotation);
        ball.transform.localScale = Vector3.one * attk.currentCharge;
        attk.currentCharge = 0f;
    }

    void Update()
    {
        
    }
}
