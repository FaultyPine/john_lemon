using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostLogic : MonoBehaviour
{
    public float health = 100f;
    public float healthDec = 50f;


    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other) {
        if (other.CompareTag("EnergyBall")) {
            health -= healthDec;
            if (health <= 0f) {
                Destroy(this.gameObject);
            }
        }
    }
}
