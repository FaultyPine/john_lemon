Boxing ring model by "3d game artist lab" on sketchfab:
https://sketchfab.com/3d-models/cartoon-boxing-ring-87eddb7fce284eac907dccba97797d96#download

John Lemon can charge up energy and shoot energy balls at oncoming ghost enemies.
The enemies move towards john in 3 lanes. If the enemy reaches john, you lose. If you survive for a certain amount
of time, you win. 
You can look at the nearest ghost by holding W, and charge/fire your energy balls with Spacebar.


3 Features:
 - John Lemon can shoot energy balls that defeat ghosts
 - Ghosts spawn in 3 "lanes" (at different positions) and move towards John Lemon to defeat him
 - If you survive a certain amount of "waves" (on a timer) of ghosts, you win. If they touch you, you lose.